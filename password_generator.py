# -*- coding : utf8 -*-

import random 



def ExitOnError():
	print ("Erreur,faites Entrer pour quitter le programme")
	input()
	sys.exit(0)

def verifierExiste (file):
	fichier = Path(file)

	if fichier.is_file() == False:
		print ("Erreur, le fichier est introuvable")
		ExitOnError()

def lireFichier(file):
	fichier = open(file, "r")
	contenue = ""
	for caractere in fichier:
		contenue += caractere

	if contenue == "" :
		print ("Erreur fichier vide")
		ExitOnError()
	else :
	
		return contenue	


# TEST
verifierExiste("caracteres.txt")
mes_caracteres = lireFichier("caracteres.txt")
print("Mes caractères sont : "+mes_caracteres)
